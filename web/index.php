<!DOCTYPE html>
<html lang="en">
  <head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="js/sorttable.js"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>


    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  </head>

  <body>



    <style>
      table.sortable th:not(.sorttable_sorted):not(.sorttable_sorted_reverse):not(.sorttable_nosort):after { 
        content: "\00A0\25B4\25BE" 
      }
/*     tr.evententry {
       cursor: pointer;
     }
*/
     .hidden, .hiddenteilnehmer, .hiddenzeit, .hiddenentfernung, .hiddenongoing {
       display: none;
     }

     label {
       margin-right:5px;
     }

     .label-ongoing {
       font-size: 9px;
       background-color: #5cb85c;
       padding: 2px;
       border-radius: .25em;
     }

       .map {
        height: 400px;
        width: 100%;
       }

       #grossekarte {
        height: 600px;
        width: 100%;
        display: none;
       }

    </style>

    <div style="padding-right:10px;padding-left:2px;">
    <h1>Was los, digga!</h1>
    <input type="text" id="datepicker">
    <label><input type="checkbox" id="meetupcheck" checked>Meetup</label>
    <label><input type="checkbox" id="facebookcheck" checked>Facebook</label>
    <label><input type="checkbox" id="ongoingcheck" checked> <span class="label-ongoing">Ongoing</span></label>
    <label><input type="number" id="tnnum" value="10" style="width:3em;"> Min. Teilnehmer</label>
    <label><input type="checkbox" id="fruehereevents"> Ganzer Tag</label>
    <label><input type="checkbox" id="entfernungcheck">Max. Entfernung: <input type="number" id="koordnum" style="width:2em;" value="2">km</label>
    <label><input type="checkbox" id="zeigekarte"> Große Karte</label>
    <label>Dein Standpunkt: <input type="input" id="koordinaten" value="52.5218675,13.40346"></label>
    <label>Suche: <input type="input" id="suche" value=""><button type="button" id="btnsuche">Go!</button></label>
    <div style="float:right;">
      <label id="anzahlevents"></label>
    </div>
    </div>

<script>

function update_rows_fruehereevents() {
    var dt = new Date()
    var time = (dt.getHours()<10?'0':'') + dt.getHours() + ":" + (dt.getMinutes()<10?'0':'') + dt.getMinutes()

    if ($("#fruehereevents").is(':checked')) {
      $("td.tdstart").filter(function () {
        return $(this).text() < time;
      }).closest('tr').removeClass("hiddenzeit");
    } else {
      $("td.tdstart").filter(function () {
        return $(this).text() < time;
      }).closest('tr').addClass("hiddenzeit");
    }
    updateGrosseKarte();
}

function update_rows_teilnehmercheck() {
    $("td.teilnehmertd").closest('tr').removeClass("hiddenteilnehmer");
    $("td.teilnehmertd").filter(function () {
      return parseInt($($(this)).text()) < parseInt($("#tnnum").val());
    }).closest('tr').addClass("hiddenteilnehmer");

    updateGrosseKarte();
}


function update_rows_ongoing() {
  if ($("#ongoingcheck").is(':checked')) {
    $("span.label-ongoing").closest('tr').removeClass("hiddenongoing");
  } else {
    $("span.label-ongoing").closest('tr').addClass("hiddenongoing");
  }
  updateGrosseKarte();
}



function update_rows_entfernung() {
  $( "tr.evententry" ).each(function( index ) {
    $(this).removeClass("hiddenentfernung");
    lat = parseFloat($(this).closest('tr').find('.tdort').data("lat"));
    lon = parseFloat($(this).closest('tr').find('.tdort').data("lon"));
    myloc_lat = $("#koordinaten").val().split(",")[0];
    myloc_lan = $("#koordinaten").val().split(",")[1];
    if (distance(lat,lon,myloc_lat,myloc_lan,"K") > $("#koordnum").val()) {
      $(this).addClass("hiddenentfernung");
    }
  });

  updateGrosseKarte();
}


var prev_infowindow;
var timeoutId;
function updateGrosseKarte() {

  $("#anzahlevents").html("Angezeigte Events: <strong><u>" + $(".evententry:visible").length + "</u></strong>");

  if (!$("#zeigekarte").is(':checked')) {
    return;
  }

  element = $("#grossekarte")[0];
  lat_inp = parseFloat($("#koordinaten").val().split(",")[0]);
  lon_inp = parseFloat($("#koordinaten").val().split(",")[1]);

   var uluru = {lat: lat_inp, lng: lon_inp};
   var map = new google.maps.Map(element, {
     zoom: 14,
     center: uluru,
     mapTypeControl: false,
     scrollwheel: false
   });

   var marker = new google.maps.Marker({
     position: uluru,
     map: map
   });
   marker.setIcon('http://maps.google.com/mapfiles/ms/icons/blue-dot.png')

    var noPoi = [
      {
        featureType: "poi",
        stylers: [
          { visibility: "off" }
        ]   
      }
    ];

  //map.setOptions({styles: noPoi});


  $( "tr.evententry:visible" ).each(function( index ) {
    lat = parseFloat($(this).closest('tr').find('.tdort').data("lat"));
    lon = parseFloat($(this).closest('tr').find('.tdort').data("lon"));

    //NEED FIX - Bad way to extract text from tdname + description span in same element
    event_name = $(this).closest('tr').find('#tdname').clone().children().remove().end().text();
    var uluru = {lat: lat, lng: lon};
    var marker = new google.maps.Marker({
      position: uluru,
      map: map,
      //title: event_name
    });


    var title = event_name;
    var short_info = $($(this).closest('tr').find('#tdname').children()[0]).text().substring(0,300);
    var uhrzeit = $(this).closest('tr').find('.tdstart').text();
    var teilnehmer = $(this).closest('tr').find('.teilnehmertd').text();
    var mehr_infos = $(this).closest('tr').find('.tdlink').find('a').attr("href")
//    var contentString = "<h5>"+title+"</h5><p stlye='font-size:12px;color:#ddd;'>"+uhrzeit+" - Teilnehmer: "+teilnehmer+"</p>"+"<p>"+short_info+"... <a href='"+mehr_infos+"' target='_blank'>[Mehr Infos]</a></p>";
    var contentString = "<h5>"+title+"</h5><p stlye='font-size:12px;color:#ddd;'>"+uhrzeit+" - Teilnehmer: "+teilnehmer+"</p>"+"<p>"+short_info+"...</p>";

    if ($(this).find("span.label-ongoing").length !== 0) {
      marker.setIcon('https://chart.googleapis.com/chart?chst=d_map_spin&chld=0.8|0|5cb85c|14|_|'+teilnehmer)
    } else {
      marker.setIcon('https://chart.googleapis.com/chart?chst=d_map_spin&chld=0.8|0|FD7567|14|_|'+teilnehmer)
    }

    marker.infowindow = new google.maps.InfoWindow({
      content: contentString
    });

    marker.addListener('mouseover', function() {
      if(prev_infowindow) {prev_infowindow.close()};
      marker.infowindow.open(map, marker);
      prev_infowindow = marker.infowindow;
    });

    marker.addListener('mouseout', function() {
      marker.infowindow.close();
    });

    marker.addListener('click', function() {
      var win = window.open(mehr_infos, '_blank');
      if (win) {
        //Browser has allowed it to be opened
        //win.focus();
      } else {
        //Browser has blocked it
        alert('Please allow popups for this website');
      }
      marker.infowindow.close();
    });


    google.maps.event.addListener(map, "click", function(event) {
      marker.infowindow.close();
    });

  });   

}



function initMap(element, lat_inp, lon_inp) {
   var uluru = {lat: lat_inp, lng: lon_inp};
   var map = new google.maps.Map(element, {
     zoom: 15,
     center: uluru,
     mapTypeControl: false
   });
   var marker = new google.maps.Marker({
     position: uluru,
     map: map
   });
}


var x = $("#koordinaten");
function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else { 
        x.val("Geolocation is not supported by this browser.");
    }
}

function showPosition(position) {
    x.val(position.coords.latitude + "," + position.coords.longitude);
}


function distance(lat1, lon1, lat2, lon2, unit) {
  var radlat1 = Math.PI * lat1/180;
  var radlat2 = Math.PI * lat2/180;
  var theta = lon1-lon2;
  var radtheta = Math.PI * theta/180;
  var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
  dist = Math.acos(dist);
  dist = dist * 180/Math.PI;
  dist = dist * 60 * 1.1515;
  if (unit=="K") { dist = dist * 1.609344 }
  if (unit=="N") { dist = dist * 0.8684 }
  return dist
}






$(function() {

  //get the location
  getLocation();

  $( "#datepicker" ).datepicker({ 
    dateFormat: 'yy/mm/dd',
    maxDate: "+1w",
    minDate: "0"
  });
  $( "#datepicker" ).datepicker('setDate', new Date());

  var now = new Date();
  var day = ("0" + now.getDate()).slice(-2);
  var month = ("0" + (now.getMonth() + 1)).slice(-2);
  var today = now.getFullYear()+"-"+(month)+"-"+(day);
//  $("#datepicker").val(today);

  
  $('#meetupcheck').on('change', function() {
    if ($("#meetupcheck").is(':checked')) {
      $('tr.Meetup').removeClass("hidden");
    } else {
      $('tr.Meetup').addClass("hidden");
    }
    updateGrosseKarte();
  });

  $('#facebookcheck').on('change', function() {
    if ($("#facebookcheck").is(':checked')) {
      $('tr.Facebook').removeClass("hidden");
    } else {
      $('tr.Facebook').addClass("hidden");
    }
    updateGrosseKarte();
  });

  $('#entfernungcheck').on('change', function() {
    if ($("#entfernungcheck").is(':checked')) {
      update_rows_entfernung();
    } else {
      $('tr.Facebook, tr.Meetup').removeClass("hiddenentfernung");
      updateGrosseKarte();
    }
    updateGrosseKarte();
  });



  $('#zeigekarte').on('change', function() {
    if ($("#zeigekarte").is(':checked')) {
      $("#grossekarte").show();
      updateGrosseKarte();
    } else {
      $("#grossekarte").hide();
    }
  });



  $('#teilnehmercheck').on('change', function() {
    update_rows_teilnehmercheck();
  });

  $('#tnnum').on('change', function() {
    update_rows_teilnehmercheck();
  });

  $('#fruehereevents').on('change', function() {
    update_rows_fruehereevents();
  });

  $('#koordnum').on('change', function() {
    update_rows_entfernung();
  });

  $('#ongoingcheck').on('change', function() {
    update_rows_ongoing();
  });

  $('#suche').keypress(function (e) {
    if (e.which == 13) {
      $('#btnsuche').click();
      return false;    //<---- Add this line
    }
  });

  $('#btnsuche').on('click', function() {
    if ($("#suche").val() != "") {
      $("#content").empty();

      $.ajax({
        url: "getevents.php",
        data: { 
          "q": $("#suche").val() 
        },
        cache: false,
        success: function(html){
          $("#content").append(html);
          var newTableObject = document.getElementById("eventstable");
          sorttable.makeSortable(newTableObject);

          $("tbody").on("click", "td.tdname_click", function(e){
            $desc = $(this).closest('tr').find('span#desc').html();
            $nexttr = $(this).closest('tr').next('tr');
            if ($nexttr.hasClass("desctr")) {
              $nexttr.remove();
            } else {
              $(this).closest('tr').after('<tr style="background-color:#ccc;" class="desctr"><td colspan=3></td><td colspan=1><div style="max-width:500px;font-size:14px;">'+$desc+"</div></td><td colspan=3><div class='map'></div></td></tr>");
              lat = parseFloat($(this).closest('tr').find('.tdort').data("lat"));
              lon = parseFloat($(this).closest('tr').find('.tdort').data("lon"));
              if (lat != 0) {            
                initMap($(this).closest('tr').next().find("div.map")[0], lat, lon);
              }
            }
          });

          $("#tnnum").val("0");
          update_rows_teilnehmercheck();
          update_rows_fruehereevents();
          $("#anzahlevents").html("Angezeigte Events: <strong><u>" + $(".evententry:visible").length + "</u></strong>");
        }
      });
      
    }
  });



  $.ajax({
    url: "getevents.php",
    cache: false,
    success: function(html){
      $("#content").append(html);
      var newTableObject = document.getElementById("eventstable");
      sorttable.makeSortable(newTableObject);

      $("tbody").on("click", "td.tdname_click", function(e){
        $desc = $(this).closest('tr').find('span#desc').html();
        $nexttr = $(this).closest('tr').next('tr');
        if ($nexttr.hasClass("desctr")) {
          $nexttr.remove();
        } else {
          $(this).closest('tr').after('<tr style="background-color:#ccc;" class="desctr"><td colspan=3></td><td colspan=1><div style="max-width:500px;font-size:14px;">'+$desc+"</div></td><td colspan=3><div class='map'></div></td></tr>");
          lat = parseFloat($(this).closest('tr').find('.tdort').data("lat"));
          lon = parseFloat($(this).closest('tr').find('.tdort').data("lon"));
          if (lat != 0) {            
            initMap($(this).closest('tr').next().find("div.map")[0], lat, lon);
          }
        }
      });

      update_rows_teilnehmercheck();
      update_rows_fruehereevents();
      $("#anzahlevents").html("Angezeigte Events: <strong><u>" + $(".evententry:visible").length + "</u></strong>");
    }
  });


  $('#datepicker').change(function(){
      $("#content").empty();

      $.ajax({
        url: "getevents.php",
        method: "GET",
        data: { date: $("#datepicker").val() },
        cache: false,
        success: function(html){
          $("#content").append(html);
          var newTableObject = document.getElementById("eventstable");
          sorttable.makeSortable(newTableObject);

          $("tbody").on("click", "td.tdname_click", function(e){
            $desc = $(this).closest('tr').find('span#desc').html();
            $nexttr = $(this).closest('tr').next('tr');
            if ($nexttr.hasClass("desctr")) {
              $nexttr.remove();
            } else {
              $(this).closest('tr').after('<tr style="background-color:#ccc;" class="desctr"><td colspan=3></td><td colspan=1><div style="max-width:500px;font-size:14px;">'+$desc+"</div></td><td colspan=3><div class='map'></div></td></tr>");
              lat = parseFloat($(this).closest('tr').find('.tdort').data("lat"));
              lon = parseFloat($(this).closest('tr').find('.tdort').data("lon"));
              if (lat != 0) {            
                initMap($(this).closest('tr').next().find("div.map")[0], lat, lon);
              }
            }
          });

          update_rows_teilnehmercheck();
          update_rows_fruehereevents();
          update_rows_ongoing();
          $("#anzahlevents").html("Angezeigte Events: <strong><u>" + $(".evententry:visible").length + "</u></strong>");

        }
      });
  });


});

</script>

<div id="grossekarte">
</div>


<div id="content">
</div>


    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=YOURAPIKEY">
    </script>


  </body>
</html>
