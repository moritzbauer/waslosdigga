<?php


echo'    <table class="table sortable" id="eventstable">
      <thead class="thead-inverse">
        <tr>
        <th class="">Datum</th>
        <th>Startzeit</th>
        <th class="sorttable_nosort">Mehr&nbsp;Infos</th>
        <th class="sorttable_nosort">Name</th>
        <th>TN</th>
        <th>W/M</th>
        <th>Ort</th>
      </tr>
     </thead>
     <tbody id="tblcontent"> ';



    # CHANGE PATH TO DB FILE
    $myPDO = new PDO('sqlite:/PATH/TO/meetups.db');
    //$result = $myPDO->query("SELECT title as Name, DATE_FORMAT(start_time, '%d/%m') as Datum, DATE_FORMAT(start_time, '%H:%i') as Startzeit, details as Beschreibung, attendees as Teilnehmer, attendees_male as Male, attendees_female as Female, (round(Female*100/(Male+Female),0)) as Percentage, location as Ort, address as Adresse, link as URL FROM meetups WHERE DATE(Datum) = CURDATE() GROUP BY Name ORDER BY Startzeit");

    $sql = "SELECT title AS Name,
        strftime('%d/%m', start_time) AS Datum,
        strftime('%H:%M', start_time) AS Startzeit,
        details AS Beschreibung,
        attendees AS Teilnehmer,
        attendees_male AS Male,
        attendees_female AS Female,
        google_maps_link AS Googlemaps,
        google_calendar_link AS Googlecalendar,
        ROUND(0) AS Source,
        strftime('%d/%m', start_time) AS Enddatum,
        strftime('%H:%M', start_time) AS Endzeit,
        ROUND(attendees_female * 100 / (attendees_male + attendees_female), 0) AS Percentage,
        location AS Ort,
        address AS Adresse,
        link AS URL
        FROM meetups
        WHERE DATE(start_time) >= date('now')
        GROUP BY Name
        ORDER BY Datum, Startzeit;
    ";




    if( isset($_GET['q']) ) {
      //$sql="SELECT Name, DATE_FORMAT(Datum, '%d/%m') as Datum, DATE_FORMAT(Startzeit, '%H:%i') as Startzeit, DATE_FORMAT(Enddatum, '%d/%m') as Enddatum, DATE_FORMAT(Endzeit, '%H:%i') as Endzeit, Beschreibung, Teilnehmer, Male, Female, (round(Female*100/(Male+Female),0)) as Percentage, Lat, Lon, Ort, Adresse, URL, Source FROM Events WHERE DATE(Datum) = CURDATE() AND (Beschreibung LIKE '%".$_GET['q']."%' OR Name LIKE '%".$_GET['q']."%' OR Ort LIKE '%".$_GET['q']."%') GROUP BY Name ORDER BY Datum";
        $sql = "SELECT title AS Name,
        strftime('%d/%m', start_time) AS Datum,
        strftime('%H:%M', start_time) AS Startzeit,
        details AS Beschreibung,
        attendees AS Teilnehmer,
        attendees_male AS Male,
        attendees_female AS Female,
        google_maps_link AS Googlemaps,
        google_calendar_link AS Googlecalendar,
        ROUND(0) AS Source,
        strftime('%d/%m', start_time) AS Enddatum,
        strftime('%H:%M', start_time) AS Endzeit,
        ROUND(attendees_female * 100 / (attendees_male + attendees_female), 0) AS Percentage,
        location AS Ort,
        address AS Adresse,
        link AS URL
        FROM meetups
        WHERE (Beschreibung LIKE '%".$_GET['q']."%' OR Name LIKE '%".$_GET['q']."%' OR Ort LIKE '%".$_GET['q']."%')
        GROUP BY Name
        ORDER BY Datum, Startzeit;
        ";
    } else if( isset($_GET['date']) ) {
      $sql = "SELECT title AS Name,
        strftime('%d/%m', start_time) AS Datum,
        strftime('%Y/%m/%d', start_time) AS DatumCompare,
        strftime('%H:%M', start_time) AS Startzeit,
        details AS Beschreibung,
        attendees AS Teilnehmer,
        attendees_male AS Male,
        attendees_female AS Female,
        google_maps_link AS Googlemaps,
        google_calendar_link AS Googlecalendar,
        ROUND(0) AS Source,
        strftime('%d/%m', start_time) AS Enddatum,
        strftime('%H:%M', start_time) AS Endzeit,
        ROUND(attendees_female * 100 / (attendees_male + attendees_female), 0) AS Percentage,
        location AS Ort,
        address AS Adresse,
        link AS URL
        FROM meetups
        WHERE DatumCompare = '" . $_GET['date'] . "'
        GROUP BY Name
        ORDER BY Startzeit;
    ";
    }

    $result = $myPDO->query($sql);


    foreach($result as $row){
      //$row['lat'] = 0;
      //$row['lon'] = 0;
      //$row['Enddatum'] = 0;
    $google_maps_link = $row['Googlemaps'];
    if ($google_maps_link != "Unknown") {
        $google_maps_link = explode('query=', $google_maps_link)[1];
        $row['Lat'] = explode('%2C%20', $google_maps_link)[0];
        $row['Lon'] = explode('%2C%20', $google_maps_link)[1];
    } else {
        $row['Lat'] = 0;
        $row['Lon'] = 0;
    }


      if (is_null($row['Enddatum']) or is_null($row['Endzeit'])) {
        $hours = "1";
      } else {
        $format = 'd/m H:i';
        $sdate = DateTime::createFromFormat($format, $row['Datum']." ".$row['Startzeit']);
        $edate = DateTime::createFromFormat($format, $row['Enddatum']." ".$row['Endzeit']);
        
        $tdiff = date_diff($edate,$sdate);

        $hours = $tdiff->h;
        $hours = $hours + ($tdiff->days*24);
      }

      if ($hours > "24") {
        $ongoing = ' <span class="label-ongoing">Ongoing</span>';
      } else {
        $ongoing = '';
      }

      if ($row['Source'] == "Facebook") {
        echo '<tr class="evententry '.$row['Source'].'"><td>' . $row['Datum'] . "</td><td class='tdstart'>" . $row['Startzeit'] . "</td><td class='tdlink'><a href='".$row['URL']."' target='_blank'><img src='img/fb_s.png'/></a>".$ongoing."</td><td id='tdname' class='tdname_click' style='cursor:pointer;'><span id='desc' style='display:none;'>".$row['Beschreibung']."</span>" .$row['Name'] . "</td><td class='teilnehmertd'>" .$row['Teilnehmer'] . "</td><td class='wmtd'>".$row['Percentage']."%</td><td class='tdort' data-lat=".$row['Lat']." data-lon=".$row['Lon'].">" .$row['Ort'] . " / " .$row['Adresse'] . "</td></tr>";
      } else {
        echo '<tr class="evententry '.$row['Source'].'"><td>' . $row['Datum'] . "</td><td class='tdstart'>" . $row['Startzeit'] . "</td><td class='tdlink'><a href='".$row['URL']."' target='_blank'><img src='img/meetup_s.png'/></a>&nbsp;<a href='".$row['Googlecalendar']."' target='_blank'><img src='img/calendar.png'/></a>".$ongoing."</td><td id='tdname' class='tdname_click' style='cursor:pointer;'><span id='desc' style='display:none;'>".$row['Beschreibung']."</span>" .$row['Name'] . "</td><td class='teilnehmertd'>" .$row['Teilnehmer'] . "</td><td class='wmtd'>".$row['Percentage']."%</td><td class='tdort' data-lat=".$row['Lat']." data-lon=".$row['Lon'].">" .$row['Ort'] . " / " .$row['Adresse'] . "</td></tr>";
      }
    }

    // Free result set
    //mysqli_free_result($result);
    //mysqli_close($con);


echo '      </tbody>
    </table>';

?>
