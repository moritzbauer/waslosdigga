# Meetup.com Scraper

## Description: 

This scraper scrapes Meetup.com, gets all events for the next 7 days and displays all of them in a nice and tidy table. You can sort by date/time, number of attendees, female/male ratio. Link to meetup & "add to Google Calendar" features are available as well. Click on one of the meetup rows to expand the row, get more info about the meetup and see a map where the meetup takes place. Enjoy!


## Installation

1. Install requirements with: `pip install -r requirements.txt`
2. Change MEETUP_USERNAME & MEETUP_PASSWORD in scrape.py
2. Change PATH TO DB FILE in getevents.php 
3. Change Google Maps API Key in index.php
4. Change "Dein Standpunkt" in index.php
4. run scraper with: python scrape.py
5. See overview of events at web/index.php

## Screenshot

![scraper frontend](https://i.imgur.com/ykDRNNA.png)

