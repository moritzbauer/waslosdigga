# -*- coding: utf-8 -*-
# scrape.py

from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.keys import Keys
import selenium.common.exceptions as exceptions
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.common.by import By

from bs4 import BeautifulSoup
import re
from datetime import datetime as datetime_new
import datetime
import time
import sqlite3

import gender_guesser.detector as gender
from nameparser import HumanName


MEETUP_USERNAME = "example@example.com"
MEETUP_PASSWORD = "123456789"



def get_db():
	con = sqlite3.connect("meetups.db")
	cur = con.cursor()
	cur.execute('''
	    CREATE TABLE IF NOT EXISTS meetups (
	        id INTEGER PRIMARY KEY AUTOINCREMENT,
	        title TEXT NOT NULL,
	        start_time DATETIME,
	        link TEXT,
	        is_online INTEGER,
	        attendees INTEGER,
	        attendees_male INTEGER,
	        attendees_female INTEGER,
	        location TEXT,
	        address TEXT,
	        details TEXT,
	        google_maps_link TEXT,
	        google_calendar_link TEXT
	    )
	''')
	con.commit()
	return con

def get_bot(proxy_host=None, proxy_port=None, headless=False):
	""" function to create a bot and return it """
	chrome_options = Options()
	if proxy_host and proxy_port:
		chrome_options.add_argument(f'--proxy-server={proxy_host}:{proxy_port}')

	if headless:
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--disable-dev-shm-usage')

	chrome_options.add_argument("start-maximized")
	# setting window size
	chrome_options.add_argument("--window-size=1920,1080")
	chrome_options.add_experimental_option("excludeSwitches", ["enable-automation"])
	chrome_options.add_experimental_option('useAutomationExtension', False)

	bot = webdriver.Chrome(options=chrome_options, service=ChromeService(ChromeDriverManager().install()))
	bot.maximize_window()
	return bot


def get_meetups(bot, start, end, con, detect):
	datetime_format = "%Y-%m-%dT%H:%M:%S"
	bot.get('https://www.meetup.com/de-DE/find/?customStartDate='+start+'T18%3A00%3A00-04%3A00&customEndDate='+end+'T17%3A59%3A00-04%3A00&source=EVENTS&location=de--berlin&sortField=DATETIME')
	time.sleep(5)

	# scrolling till we reach the bottom of the page
	while True:
		current_scroll_height = bot.execute_script("return window.pageYOffset;")
		page_height = bot.execute_script("return document.body.scrollHeight;")
		if current_scroll_height and page_height:
			if current_scroll_height >= page_height - 1400:
				break
		for i in range(2):
			bot.find_element(By.TAG_NAME, 'body').send_keys(Keys.PAGE_DOWN)
			time.sleep(1)

	page_source = bot.page_source
	soup = BeautifulSoup(page_source, 'lxml')

	items = soup.find_all('div', attrs={'data-element-name' : 'categoryResults-eventCard'})
	events = []

	for item in items:
		event = {}
		event['title'] = item.find('h2').text.strip()
		event['start_time'] = datetime_new.strptime(item.find('time')['datetime'][:19], datetime_format)
		event['link'] = item.find('a')['href']
		event['id'] = event['link'].split("/")[-2]

		is_online = item.find('div', attrs={'aria-label': 'Online-Event'})
		if is_online:
			event['is_online'] = 1
		else:
			event['is_online'] = 0
		event['attendees'] = item.find('div', attrs={'aria-label': re.compile(r".*Teilnehmer")}).text.replace('Teilnehmer','').strip()
		events.append(event)

		cur = con.cursor()
		cur.execute('SELECT * FROM meetups WHERE link = ?', (event['link'],))
		existing_event = cur.fetchone()

		if existing_event:
			print('Event already exists. Updating attendees. ' + str(event['link']))
			cur.execute('''
				UPDATE meetups SET 
					attendees = :attendees
				WHERE link = :link 
			''', event)
			con.commit()
			if int(event['attendees']) - int(existing_event[5]) > 5:
				print("Attendee number change bigger than 5. Updating gender ratio...")
				scrape_attendees(bot, event['link'], con, detect)
		else:
			cur.execute('''
				INSERT INTO meetups (
					title, start_time, link, is_online, attendees
				) VALUES (
					:title, :start_time, :link, :is_online, :attendees
				)
			''', event)
			con.commit()


def get_meetup_details(link, con):
	bot.get(link)
	page_source = bot.page_source
	soup = BeautifulSoup(page_source, 'lxml')

	event = {}
	event["link"] = link

	if soup.find('a', attrs={'data-event-label': 'event-location'}):
		event['location'] = soup.find('a', attrs={'data-event-label': 'event-location'}).text.strip()
	else:
		event['location'] = 'Unknown'
	if soup.find('a', attrs={'data-event-label': 'event-location'}) and soup.find('a', attrs={'data-event-label': 'event-location'}).next_sibling:
		event['address'] = soup.find('a', attrs={'data-event-label': 'event-location'}).next_sibling.text.strip()
	else:
		event['address'] = 'Unknown'
	if soup.find('div', attrs={'id': 'event-details'}):
		event['details'] = soup.find('div', attrs={'id': 'event-details'}).text[7:].strip()
	else:
		event['details'] = 'Unknown'
	if soup.find('a', attrs={'data-event-label': 'event-location'}):
		event['google_maps_link'] = soup.find('a', attrs={'data-event-label': 'event-location'})['href']
	else:
		event['google_maps_link'] = 'Unknown'
	if soup.find('a', attrs={'data-event-label': 'add-event-to-calendar-google'}):
		event['google_calendar_link'] = soup.find('a', attrs={'data-event-label': 'add-event-to-calendar-google'})['href']
	else:
		event['google_calendar_link'] = "Unknown"


	#if soup.find('h2', text=re.compile("Attendees")):
	#	event['attendees'] = soup.find('h2', text=re.compile("Attendees")).text.replace("Attendees",'').replace("(",'').replace(')','').strip()
	#else:
	#	event['attendees'] = 0

	# Check if the event already exists based on its link
	cur = con.cursor()
	cur.execute('SELECT * FROM meetups WHERE link = ?', (event["link"],))
	existing_event = cur.fetchone()

	if existing_event:
		cur.execute('''
			UPDATE meetups SET 
				location = :location, 
				address = :address, 
				details = :details, 
				google_maps_link = :google_maps_link,
				google_calendar_link = :google_calendar_link
			WHERE link = :link 
		''', event)
		con.commit()

	return


def update_meetups(con):
	cur = con.cursor()

	# Check if the event already exists based on its link
	cur.execute('SELECT * FROM meetups WHERE location IS NULL')
	events = cur.fetchall()

	cnt = 1
	total = len(events)
	for event in events:
		print("[" + str(cnt) + " / " + str(total) + "] Updating meetup: " + event[3])
		get_meetup_details(event[3], con)
		#time.sleep(2)
		cnt += 1


def update_attendee_ratio(bot, con, detect):
	# Check if the event already exists based on its link
	cur = con.cursor()
	cur.execute('SELECT * FROM meetups WHERE attendees_male IS NULL')
	events = cur.fetchall()

	cnt = 1
	total = len(events)
	for event in events:
		print("[" + str(cnt) + " / " + str(total) + "] Updating meetup gender ratio: " + event[3])
		try:
			scrape_attendees(bot, event[3], con, detect)
		except Exception as e:
			print("Error: "+str(e))
			continue
		time.sleep(2)
		cnt += 1
	return


def scrape_attendees(bot, link, con, detect):
	bot.get(link+"attendees/")
	time.sleep(3)
	page_source = bot.page_source
	soup = BeautifulSoup(page_source, 'lxml')
	attendees = soup.find("ul", {"class": "attendees-list"}).find_all("h4")
	attendee_names = [attendee.text for attendee in attendees]
	gender_attendees = [detect.get_gender(HumanName(attendee).first) for attendee in attendee_names]
	m_cnt = gender_attendees.count('male')
	f_cnt = gender_attendees.count('female')

	event = {}
	event['link'] = link
	event['attendees_male'] = m_cnt
	event['attendees_female'] = f_cnt

	cur = con.cursor()
	cur.execute('SELECT * FROM meetups WHERE link = ?', (event['link'],))
	existing_event = cur.fetchone()
	if existing_event:
		cur.execute('''
			UPDATE meetups SET 
				attendees_male = :attendees_male, 
				attendees_female = :attendees_female
			WHERE link = :link 
		''', event)
		con.commit()


def meetup_login(bot):
	bot.get("https://meetup.com/login/")
	bot.find_element("id", "email").send_keys(MEETUP_USERNAME)
	bot.find_element("id", "current-password").send_keys(MEETUP_PASSWORD)
	bot.find_element(By.CSS_SELECTOR, '[name="submitButton"]').click()
	time.sleep(7)



if __name__ == '__main__':
	print("Starting up...")
	con = get_db()
	bot = get_bot(None, None, True)
	detect = gender.Detector()

	start_day = datetime.datetime.now().date() - datetime.timedelta(days=1)
	next_day = start_day + datetime.timedelta(days=1)

	# login to meetup // needed to get attendees and google calendar link
	print("Logging into meetup")
	meetup_login(bot)

	# get meetups for the next 7 days
	print("Get meetups for the next 7 days...")
	for i in range(7):
		print(next_day.strftime("%Y-%m-%d"))
		get_meetups(bot, start_day.strftime("%Y-%m-%d"), next_day.strftime("%Y-%m-%d"), con, detect)
		start_day = next_day
		next_day = next_day + datetime.timedelta(days=1)

	# get details of meetups
	print("#################################################")
	print("Get details of meetups")
	update_meetups(con)

	# update gender ratio where no ratio is present yet
	print("#################################################")
	print("Update gender ratio where no ratio is present yet")
	update_attendee_ratio(bot, con, detect)


